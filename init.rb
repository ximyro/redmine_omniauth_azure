require 'redmine'
require_dependency 'redmine_omniauth_azure/hooks'
Redmine::Plugin.register :redmine_omniauth_azure do
  name 'Redmine Omniauth Azure plugin'
  author 'Ilya Buzlov'
  description 'This is a plugin for Redmine registration through azure'
  version '0.0.1'
  author_url 'http://vk.com/ionipersii'

  settings :default => {
    :client_id => "",
    :client_secret => "",
    :oauth_autentification => false,
    :tenat => ""
  }, :partial => 'settings/azure_settings'
end
