require 'account_controller'
require 'jwt'

class RedmineOauthController < AccountController
  include Helpers::MailHelper
  def oauth_azure
    if Setting.plugin_redmine_omniauth_azure[:oauth_authentification]
      session[:back_url] = params[:back_url]
      redirect_to oauth_client.auth_code.authorize_url(:redirect_uri => oauth_azure_callback_url,:resource => '00000002-0000-0000-c000-000000000000')
    else
      password_authentication
    end
  end

  def oauth_azure_callback
      access_token = oauth_client.auth_code.get_token(params[:code], :redirect_uri => oauth_azure_callback_url)
      result = ::JWT.decode(access_token.token, nil, false).first
      if result && result['aud']
          try_to_login result
      else
        flash[:error] = result['error_description']
        redirect_to signin_path
      end
  end

  def try_to_login info
   params[:back_url] = session[:back_url]
   session.delete(:back_url)
   user = EmailAddress.find_or_initialize_by(:address => info["upn"]).user || User.new
    if user.new_record?
      # Self-registration off
      redirect_to(home_url) && return unless Setting.self_registration?
      # Create on the fly
      user.firstname = info["given_name"]
      user.lastname  = info["family_name"]
      user.mail = info["upn"]
      user.login = parse_email(info["upn"])[:login]
      user.random_password
      user.register
      case Setting.self_registration
      when '1'
        register_by_email_activation(user) do
          onthefly_creation_failed(user)
        end
      when '3'
        register_automatically(user) do
          onthefly_creation_failed(user)
        end
      else
        register_manually_by_administrator(user) do
          onthefly_creation_failed(user)
        end
      end
    else
      # Existing record
      if user.active?
        successful_authentication(user)
      else
        # Redmine 2.4 adds an argument to account_pending
        if Redmine::VERSION::MAJOR > 2 or
          (Redmine::VERSION::MAJOR == 2 and Redmine::VERSION::MINOR >= 4)
          account_pending(user)
        else
          account_pending
        end
      end
    end
  end

  def oauth_client
    @client ||= OAuth2::Client.new(settings[:client_id], settings[:client_secret],
      :site => 'https://login.windows.net',
      :authorize_url => "/#{settings[:tenat]}/oauth2/authorize",
      :token_url => "/#{settings[:tenat]}/oauth2/token")
  end

  def settings
    @settings ||= Setting.plugin_redmine_omniauth_azure
  end
end
