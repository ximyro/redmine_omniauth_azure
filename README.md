## Redmine omniauth microsoft Azure

This plugin is used to authenticate Redmine users using Microsoft Azure's OAuth2 provider.

### Installation:

Download the plugin and install required gems:

```console
cd /path/to/redmine/plugins
cd /path/to/redmine
bundle install
```

Restart the app
```console
touch /path/to/redmine/tmp/restart.txt
```
### Authentication Workflow

1. An unauthenticated user requests the URL to your Redmine instance.
2. User clicks the "Login via azure" buton.
3. The plugin redirects them to a Microsoft Azure sign in page if they are not already signed in to their Microsoft Azure account.
4. Microsoft Azure redirects user back to Redmine, where the Microsoft Azure OAuth plugin's controller takes over.

One of the following cases will occur:
1. If self-registration is enabled (Under Administration > Settings > Authentication), user is redirected to 'my/page'
2. Otherwse, the an account is created for the user (referencing their Microsoft Azure OAuth2 ID). A Redmine administrator must activate the account for it to work.
